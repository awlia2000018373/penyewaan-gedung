package com.company;

public class Customer {
    protected String Cnama;
    protected String Ctanggal;

    public String getCnama() {
        return this.Cnama;
    }

    public void setCnama(String n) {
        this.Cnama = n;
    }

    public String getCtanggal() {
        return this.Ctanggal;
    }

    public void setCtanggal(String t) {
        this.Ctanggal = t;
    }
}
