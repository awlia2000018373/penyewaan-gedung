package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
	// write your code here
        String Fasilitas;
        String HargaGedung;
        String tipe;
        Scanner input = new Scanner(System.in);
        Gedung user=new Gedung();
        System.out.println();
        System.out.println("APLIKASI PENYEWAAN GEDUNG BY AWLIA.COM");
        System.out.println("Gedung yang tersedia disini memiliki fasilitas dengan harga yang bervarian");
        System.out.println();
        System.out.println("Formulir Customer");
        System.out.print("Masukan nama : ");
        String nama= input.next();
        System.out.print("Masukan tanggal sewa : ");
        String date = input.next();

        user.setCnama(nama);
        user.setCtanggal(date);
        System.out.println("Kemudian silahkan pilih type gedung sesuai kebutuhan anda");
        System.out.println("Pilihan Sewa");
        System.out.println("1. Standart");
        System.out.println("2. Delux");
        System.out.println("3. VIP");
        System.out.print(" Masukan pilihan ");
        int a= input.nextInt();
        if (a==1){
            tipe= "Standart";
            Fasilitas= "Meeting Room (200 Kursi),Musholla,Toilet";
            HargaGedung = "2000000";
        }
        else if (a==2){
            tipe="Delux";
            Fasilitas= "Meeting Room (350 Kursi),Musholla,Toilet";
            HargaGedung = "3000000";
        }
        else {
            tipe="VIP";
            Fasilitas= "Meeting Room (500 kursi),Musholla,Toilet,VIP Room";
            HargaGedung ="4000000";
        }
        user.setFasilitas(Fasilitas);
        user.setHargaGedung(HargaGedung);
        user.setTipe(tipe);
        System.out.println();
        System.out.println("===================NOTA PENYEWAAN GEDUNG=====================");
        System.out.println("=============================================================");
        System.out.println("|| Nama                         : "+user.getCnama());
        System.out.println("|| Tanggal Sewa                 : " +user.getCtanggal());
        System.out.println("|| Pilihan gedung               : "+user.getTipe());
        System.out.println("|| Fasilitas yang didapatkan    : "+user.getFasilitas());
        System.out.println("|| Total harga Sewa             : Rp. "+user.getHargaGedung());
        System.out.println("=============================================================");
        System.out.println("===========TERIMAKASIH ATAS KEPERCAYAAN ANDA=================");
        System.out.println("==========================AWLIA.COM==========================");
    }
}
